
import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App.js";
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux';



const Root = () => (
    <Router>
      <Route path="/" component={App} />
    </Router>
  )

ReactDOM.render(<Root/>, document.getElementById("root"));