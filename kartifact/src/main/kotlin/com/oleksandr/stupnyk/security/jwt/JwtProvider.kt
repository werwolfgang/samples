package com.oleksandr.stupnyk.security.jwt

import com.oleksandr.stupnyk.security.service.UserPrincipal
import io.jsonwebtoken.*
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.security.SignatureException
import org.springframework.security.core.Authentication
import java.util.*


@Component
class JwtProvider(
    @Value("\${security.jwtSecret}") val jwtSecret: String,
    @Value("\${security.jwtExpiration}") val jwtExpiration: Long
) {

    var logger = com.oleksandr.stupnyk.utils.logger<JwtProvider>();

    fun generateJwtToken(authentication: Authentication): String {

        val userPrincipal = authentication.getPrincipal() as UserPrincipal

        val now = Date()
        val expiration = Date(now.time + jwtExpiration * 1000)

        return Jwts.builder()
            .setSubject(userPrincipal.getUsername())
            .setIssuedAt(now)
            .setExpiration(expiration)
            .signWith(SignatureAlgorithm.HS512, jwtSecret)
            .compact()
    }

    fun validateJwtToken(authToken: String): Boolean {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken)
            return true
        } catch (e: SignatureException) {
            logger.error("Invalid JWT signature -> Message: {} ", e)
        } catch (e: MalformedJwtException) {
            logger.error("Invalid JWT token -> Message: {}", e)
        } catch (e: ExpiredJwtException) {
            logger.error("Expired JWT token -> Message: {}", e)
        } catch (e: UnsupportedJwtException) {
            logger.error("Unsupported JWT token -> Message: {}", e)
        } catch (e: IllegalArgumentException) {
            logger.error("JWT claims string is empty -> Message: {}", e)
        }

        return false
    }

    fun getUserNameFromJwtToken(token: String): String {
        return Jwts.parser()
            .setSigningKey(jwtSecret)
            .parseClaimsJws(token)
            .getBody().getSubject()
    }

}