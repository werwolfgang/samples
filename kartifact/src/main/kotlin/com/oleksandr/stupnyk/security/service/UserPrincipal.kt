package com.oleksandr.stupnyk.security.service

import com.oleksandr.stupnyk.entity.User
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

class UserPrincipal(

    val user_id : Long,
    val email : String,
    val first_name : String,
    val last_name : String,
    val pass : String,
    val auth : MutableCollection<out GrantedAuthority>

): UserDetails {


    companion object {
        @JvmStatic
        fun build(user : User) : UserPrincipal {
            var roles = user.roles.map { SimpleGrantedAuthority(it.role)}.toMutableList()
            return UserPrincipal(
                user.user_id,
                user.email,
                user.first_name,
                user.last_name,
                user.password,
                roles
            )
        }
    }



    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        return this.auth
    }

    override fun isEnabled(): Boolean {
        return true
    }

    override fun getUsername(): String  {
        return this.email
    }

    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    override fun getPassword(): String {
        return  this.pass
    }

    override fun isAccountNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonLocked(): Boolean {
        return true
    }

}