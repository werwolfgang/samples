package com.oleksandr.stupnyk.security.service

import com.oleksandr.stupnyk.repository.UserRepository
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.transaction.annotation.Transactional


@Service
class UserDetailsServiceImpl (var userRepository: UserRepository): UserDetailsService {

    @Transactional
    override fun loadUserByUsername(username: String): UserDetails {
        val user = userRepository.findByEmail(username).orElseThrow(
            { UsernameNotFoundException("User Not Found with -> username or email : $username") })
        return UserPrincipal.build(user)
    }
}