package com.oleksandr.stupnyk.repository

import com.oleksandr.stupnyk.entity.Role
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface RoleRepository : CrudRepository<Role, Long>