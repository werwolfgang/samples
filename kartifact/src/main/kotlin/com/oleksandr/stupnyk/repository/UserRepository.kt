package com.oleksandr.stupnyk.repository

import com.oleksandr.stupnyk.entity.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface UserRepository : JpaRepository<User, Long>{
    fun findByEmail(email: String): Optional<User>
}