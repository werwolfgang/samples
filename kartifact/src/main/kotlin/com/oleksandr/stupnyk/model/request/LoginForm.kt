package com.oleksandr.stupnyk.model.request

import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

data class LoginForm(
    @NotBlank
    @Size(min = 6, max = 60)
    var email: String,

    @NotBlank
    @Size(min = 6, max = 40)
    var password: String

)