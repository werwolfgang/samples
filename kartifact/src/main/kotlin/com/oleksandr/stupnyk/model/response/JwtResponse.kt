package com.oleksandr.stupnyk.model.response

import org.springframework.security.core.GrantedAuthority


class JwtResponse(var accessToken: String, var email: String, val authorities: Collection<GrantedAuthority>) {
    var tokenType = "Bearer"
}