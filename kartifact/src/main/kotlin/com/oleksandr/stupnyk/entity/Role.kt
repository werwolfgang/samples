package com.oleksandr.stupnyk.entity

import javax.persistence.Entity
import javax.persistence.Id

@Entity
data class Role(
    @Id
    val role: String
)