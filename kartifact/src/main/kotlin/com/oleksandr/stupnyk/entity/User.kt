package com.oleksandr.stupnyk.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

@Entity
data class User(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val user_id: Long,

    @Column
    val email: String,

    @Column
    val first_name: String,

    @Column
    val last_name: String,

    @Column
    @JsonIgnore
    val password: String,

    @ManyToMany(cascade = arrayOf(CascadeType.ALL))
    @JoinTable(name = "user_role",
        joinColumns = arrayOf( JoinColumn(name = "user_id")),
        inverseJoinColumns = arrayOf( JoinColumn(name = "role") ))
    val roles: List<Role>
)

//sEkQZQG7AsUVFKqV