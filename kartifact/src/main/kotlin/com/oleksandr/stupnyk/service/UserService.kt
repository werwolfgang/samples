package com.oleksandr.stupnyk.service

import com.oleksandr.stupnyk.entity.User
import com.oleksandr.stupnyk.repository.UserRepository
import com.oleksandr.stupnyk.utils.logger
import org.springframework.stereotype.Service

interface UserService{
    fun findAllUsers() : MutableIterable<User>
}

@Service
class UserServiceImpl (private val userRepository: UserRepository) : UserService{
    val logger = logger<UserService>()

    override fun findAllUsers(): MutableIterable<User> {
        logger.info("find all users")
        return userRepository.findAll()
    }



}