package com.oleksandr.stupnyk.controller

import com.oleksandr.stupnyk.repository.UserRepository
import com.oleksandr.stupnyk.service.UserService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api")
class UserController( private val userService: UserService){

    @GetMapping("/user")
    fun getAllUsers() = userService.findAllUsers()
}