package com.oleksandr.stupnyk.controller

import com.oleksandr.stupnyk.model.request.LoginForm
import com.oleksandr.stupnyk.model.response.JwtResponse
import com.oleksandr.stupnyk.security.jwt.JwtProvider
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.web.bind.annotation.*
import javax.validation.Valid


@RestController
@RequestMapping("/api/auth")
class AuthController (var authenticationManager: AuthenticationManager, var jwtProvider: JwtProvider){

    @PostMapping("/signin")
    fun authenticateUser(@Valid @RequestBody loginForm: LoginForm) : ResponseEntity<Any>{

        val authentication = authenticationManager.authenticate(
            UsernamePasswordAuthenticationToken(loginForm.email, loginForm.password)
        )

        SecurityContextHolder.getContext().authentication = authentication

        val jwt = jwtProvider.generateJwtToken(authentication)
        val userDetails = authentication.principal as UserDetails

        return ResponseEntity.ok(JwtResponse(jwt, userDetails.username, userDetails.authorities))

        return ResponseEntity.ok(loginForm)
    }
}