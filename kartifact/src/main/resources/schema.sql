DROP SCHEMA IF EXISTS test;

CREATE SCHEMA test;
USE test;

CREATE TABLE user(
    user_id BIGINT NOT NULL AUTO_INCREMENT,
    email VARCHAR(255),
    first_name VARCHAR(255),
    last_name VARCHAR(255),
    password VARCHAR(255)
);

ALTER TABLE user ADD CONSTRAINT user_pk PRIMARY KEY (user_id);

CREATE TABLE user_role(
    user_id BIGINT NOT NULL,
    role VARCHAR(20) NOT NULL
);


ALTER TABLE user_role ADD CONSTRAINT user_role_pk PRIMARY KEY (user_id, role);


CREATE TABLE role(
    role VARCHAR(20) NOT NULL
);

ALTER TABLE role ADD CONSTRAINT role_pk PRIMARY KEY (role);

ALTER TABLE user_role ADD CONSTRAINT user_role_user_fk FOREIGN KEY (user_id) REFERENCES user(user_id);
ALTER TABLE user_role ADD CONSTRAINT user_role_role_fk FOREIGN KEY (role) REFERENCES role(role);

