INSERT INTO user(email, first_name, last_name, password) VALUES
('admin@admin', 'admin', 'admin', '$2a$10$9vOAT.PbOPWxwbfExBKCruRygTEnbFz0BXrPNIAtMwgxuhASZxwD6'),
('user@user', 'user', 'user', '$2a$10$9vOAT.PbOPWxwbfExBKCruRygTEnbFz0BXrPNIAtMwgxuhASZxwD6');

INSERT INTO role (role) VALUES
('ROLE_USER'),
('ROLE_ADMIN');

INSERT INTO user_role VALUES
(1, 'ROLE_USER'),
(1, 'ROLE_ADMIN'),
(2, 'ROLE_USER');
